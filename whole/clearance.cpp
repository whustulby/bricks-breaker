#include "clearance.h"
#include "ui_clearance.h"
#include "startinterface.h"
#include "mainwindow.h"
Clearance::Clearance(QWidget *parent,int level) :
    QDialog(parent),
    ui(new Ui::Clearance)
{
    ui->setupUi(this);
    level_now = level;
}

Clearance::~Clearance()
{
    delete ui;
}

void Clearance::on_pushButton_clicked()
{
    StartInterface *s = new StartInterface(nullptr,true);
    s->show();
    this->close();
    this->deleteLater();
}

void Clearance::on_pushButton_2_clicked()
{
    MainWindow *m = new MainWindow(nullptr,level_now + 1);
    m->show();
    this->close();
    this->deleteLater();
}
