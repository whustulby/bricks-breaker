#include "readme.h"
#include "ui_readme.h"
#include "startinterface.h"
ReadMe::ReadMe(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReadMe)
{
    ui->setupUi(this);
    resize(int(600),int(600*1.44));
    ui->pushButton->setGeometry(0 , 0,int(0.243*600),int(0.125*600*1.44));
    ui->pushButton->setIcon(QPixmap(":/new/icons/images/icons/back.png"));
    ui->pushButton->setIconSize(QSize(int(0.243*600),int(0.125*600*1.44)));
    ui->pushButton->setStyleSheet("QPushButton{border:0px}");
    ui->textBrowser->setGeometry(int(0.2*600),int(0.2*600),int(0.6*600),int(0.6*600));
}

ReadMe::~ReadMe()
{
    delete ui;
}

void ReadMe::on_pushButton_clicked()
{
    StartInterface *s = new StartInterface(nullptr,true);
    s->show();
    this->close();
    this->deleteLater();
}

void ReadMe::paintEvent(QPaintEvent *){
    QPainter painter(this);
    QPixmap p;
    p.load(":/new/backgrounds/images/backgrounds/readmePicture.png");
    painter.drawPixmap(0,0,this->width(),this->height(),p);
}

