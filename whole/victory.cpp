#include "victory.h"
#include "ui_victory.h"
#include "startinterface.h"

victory::victory(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::victory)
{
    ui->setupUi(this);
}

victory::~victory()
{
    delete ui;
}

void victory::on_pushButton_clicked()
{
    StartInterface *s = new StartInterface(nullptr,true);
    s->show();
    this->close();
    this->deleteLater();
}
