#ifndef LEVEL_H
#define LEVEL_H

#include <QDialog>
#include "mainwindow.h"
#include <QPainter>
#include <QPaintEvent>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
namespace Ui {
class Level;
}

class Level : public QDialog
{
    Q_OBJECT

public:
    int level =1;
    int blocked[8] = {1,1,1,1,1,1,1,1};
    explicit Level(QWidget *parent = nullptr);
    ~Level();
    void paintEvent(QPaintEvent *);
private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

private:
    Ui::Level *ui;

    QLabel *block[8];
};

#endif // LEVEL_H
