#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "loseing.h"
#include "clearance.h"
#include <QMainWindow>
#include <QtGui>
#include <QLabel>
#include <QPalette>
#include <QTimer>
#include <QWidget>
#include <QString>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    friend class QNewObject;
    explicit MainWindow(QWidget *parent = nullptr,int level = 1);
    ~MainWindow();
    int level_now;//记录level
    //窗体自适应事件
    void resizeEvent(QResizeEvent*);
    //鼠标移动事件
    void mouseMoveEvent(QMouseEvent*);
    //鼠标点击事件
    void mousePressEvent(QMouseEvent*);
    //键盘方向键事件
    void keyPressEvent(QKeyEvent*);
    bool interval = false;
    //窗口部分
    double windowWidth = 600, windowHeight = windowWidth * 1.46;//ui界面宽高
    double baffleWidth = windowWidth * 11.0 / 30, baffleHeight = windowHeight * 1.0 / 32, baffleX = windowWidth * 1.0 / 3, baffleY = windowHeight * 7.0 / 8;//挡板初始宽高、初始坐标
//状态栏部分
    double healthWidth = windowWidth *0.25, healthHeight = windowHeight*0.06;
    //小球部分
    double ballRadius = windowWidth * 0.015, ballX =  baffleX + baffleWidth / 2 - ballRadius, ballY = baffleY - ballRadius * 2;//球初始半径、初始坐标
    double flashBallX,flashBallY;
    int launchBall = 0;//球是否发射
    double baffleRecordX = baffleX;//记录发射球前一瞬间，挡板位置,便于给球初速度
    double vx = 0,vy = -20;//球移动速度
    int limitX = 20,limitY = 30;//球最大移动速度
    int health = 3;//小球生命数
//障碍物部分
    double obstacleWidth = windowWidth * 0.09;//障碍物宽度
    double obstacleHeight = obstacleWidth * 0.46;//障碍物高度
    //关卡1障碍物
    int obstacleNum;//障碍物数量
    bool obstacleDestoryed[100] = { false};//记录障碍物是否被摧毁
    double obstacleX[100],obstacleY[100];//障碍物坐标
    //关卡二障碍物
//定时器参数
    int ballMoveTimerFPS =  33;//球运动定时器频率
    int collisionTimerFPS = 30;//碰撞检测定时器频率
    int clearanceTimerFPS = 1000;//通关时间定时器频率
    int flashBallTimerFPS = 100;//球影子残留定时器频率
    int flashTimes = 1;
    int limitTime;//通关时间限制
   //特殊砖块激光部分参数
    int laserCondition = 1;
    double laserWidth = baffleWidth*0.3;
    int laserFPS = 120;
private slots:
    //小球碰撞事件，定时器对应collisionTimer
    void collisionSituation();
    //小球移动，定时器对应ballMoveTimer
    void moveBallEvent();
    //点击暂停
    void on_intervalButton_clicked();
    //返回home
    void on_homeButton_clicked();
    //生命值显示
    void healthShow();
    //游戏时间倒计时
    void clearanceTimerEvent();
    //球影子残留
    void flashBallEvent();
    //特殊砖块
    void special_1();
    void special_2();
    //void special_3();
private:
    Ui::MainWindow *ui;
    QLabel *obstacleLabel[100];//对ui中的label定义同数量指针，便于操作label
    //QLabel *gameTimeLabel = new QLabel;
    //QLineEdit *timeLine = new QLineEdit;
    QTimer *healthTimer = new QTimer(this);//关卡生命值定时器
    QTimer *ballMoveTimer = new QTimer(this);//球运动定时器
    QTimer *collisionTimer = new QTimer(this);//碰撞检测
    QTimer *clearanceTimer = new QTimer(this);//通关时间限制
    QTimer *specialTimer_1 = new QTimer(this);//特殊砖块1  挡板变长变宽
    QTimer *specialTimer_2 = new QTimer(this);//特殊砖块2   挡板发射激光
    QTimer *specialTimer_3 = new QTimer(this);//特殊砖块3   增加小球个数
    QTimer *flashBallTimer = new QTimer(this);//撞击残留球影
};


#endif // MAINWINDOW_H
