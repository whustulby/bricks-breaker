#ifndef CLEARANCE_H
#define CLEARANCE_H
#include <QDialog>
namespace Ui {
class Clearance;
}
class Clearance : public QDialog
{
    Q_OBJECT
public:
    explicit Clearance(QWidget *parent = nullptr,int level = 1);
    ~Clearance();
    int level_now;
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
private:
    Ui::Clearance *ui;
};
#endif // CLEARANCE_H
