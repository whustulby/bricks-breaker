#ifndef README_H
#define README_H

#include <QDialog>
#include<QPainter>

namespace Ui {
class ReadMe;
}

class ReadMe : public QDialog
{
    Q_OBJECT

public:
    explicit ReadMe(QWidget *parent = nullptr);
    ~ReadMe();
    void paintEvent(QPaintEvent*);
private slots:
    void on_pushButton_clicked();

private:
    Ui::ReadMe *ui;
};

#endif // README_H
