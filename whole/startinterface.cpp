#include "startinterface.h"
#include "ui_startinterface.h"
#include "readme.h"
#include "warning.h"

StartInterface::StartInterface(QWidget *parent, bool haveread) :
    QDialog(parent),
    ui(new Ui::StartInterface)
{
    ui->setupUi(this);
    resize(int(600),int(600*1.44));
    ui->pushButton->setGeometry(int(0.305*600),int(0.5*600*1.44),int(0.38*600),int(0.072*600*1.44));
    ui->pushButton_3->setGeometry(int(0.305*600),int(0.5*600*1.44+0.12*600*1.44),int(0.38*600),int(0.072*600*1.44));
    ui->pushButton_2->setGeometry(int(0.305*600),int(0.5*600*1.44+2*0.12*600*1.44),int(0.38*600),int(0.072*600*1.44));

    ui->pushButton->setIcon(QPixmap(":/new/characters/images/characters/start.png"));
    ui->pushButton_3->setIcon(QPixmap(":/new/characters/images/characters/read.png"));
    ui->pushButton_2->setIcon(QPixmap(":/new/characters/images/characters/break.png"));
    ui->pushButton->setIconSize(QSize(int(0.38*600),int(0.07*600*1.44)));
    ui->pushButton_3->setIconSize(QSize(int(0.38*600),int(0.07*600*1.44)));
    ui->pushButton_2->setIconSize(QSize(int(0.38*600),int(0.07*600*1.44)));
    ui->pushButton->setStyleSheet("QPushButton{border:0px}");
    ui->pushButton_3->setStyleSheet("QPushButton{border:0px}");
    ui->pushButton_2->setStyleSheet("QPushButton{border:0px}");
    haveread_now = haveread;

}

StartInterface::~StartInterface()
{
    delete ui;
}

//开始游戏，进入选择关卡
void StartInterface::on_pushButton_clicked()
{
    if(haveread_now){
        Level *l = new Level;
        l->show();
        this->close();
    }
    else{
        Warning *w = new Warning;
        w->show();
        this->close();
    }
}

void StartInterface::on_pushButton_2_clicked()
{
    this->close();
}

void StartInterface::on_pushButton_3_clicked()
{
    ReadMe *r = new ReadMe;
    r->show();
    this->close();
}

void StartInterface::paintEvent(QPaintEvent *){
    QPainter painter(this);
    QPixmap p;
    p.load(":/new/backgrounds/images/backgrounds/startInterface.png");
    painter.drawPixmap(0,0,this->width(),this->height(),p);
}
