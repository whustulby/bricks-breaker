#ifndef LOSEING_H
#define LOSEING_H

#include <QDialog>

namespace Ui {
class Loseing;
}

class Loseing : public QDialog
{
    Q_OBJECT

public:
    explicit Loseing(QWidget *parent = nullptr);
    ~Loseing();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Loseing *ui;
};

#endif // LOSEING_H
