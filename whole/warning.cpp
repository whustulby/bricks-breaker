#include "warning.h"
#include "ui_warning.h"
#include "startinterface.h"
Warning::Warning(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Warning)
{
    ui->setupUi(this);
}

Warning::~Warning()
{
    delete ui;
}

void Warning::on_pushButton_clicked()
{
    StartInterface *s = new StartInterface;
    s->show();
    this->close();
    this->deleteLater();
}
