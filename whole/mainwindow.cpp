#include "mainwindow.h"
#include "startinterface.h"
#include "ui_mainwindow.h"
#include "victory.h"
#include<random>
#include<time.h>
#include<synchapi.h>

MainWindow::MainWindow(QWidget *parent,int level) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    level_now = level;


//状态栏部分
    //总栏
    ui->stateLabel->setPixmap(QPixmap(":/new/characters/images/characters/statePicture.png"));
    ui->stateLabel->setScaledContents(true);
    ui->stateLabel->setGeometry(0,0,int(windowWidth), int(windowHeight*0.11));
    //右home键和暂停键
    ui->homeButton->setGeometry(int(windowWidth*0.85),int(windowHeight*0.02),int(windowWidth*0.1),int(windowHeight*0.06));
    ui->intervalButton->setGeometry(int(windowWidth*0.75),int(windowHeight*0.02),int(windowWidth*0.1),int(windowHeight*0.06));
    ui->intervalButton->setFocus();
    //生命值
    ui->healthLabel->setGeometry(int(windowWidth*0.05),int(windowHeight * 0.02),int(healthWidth),int(healthHeight));
    ui->healthLabel->setPixmap(QPixmap(":/new/characters/images/characters/health4.png"));
    ui->healthLabel->setScaledContents(true);
    ui->healthLabel->raise();
    //剩余时间
    ui->timeLabel->setGeometry(int(windowWidth*0.35),int(windowHeight*0.02),int(windowHeight*0.25),int(windowHeight*0.06));
    QPalette palette;
    palette.setColor(QPalette::Background, QColor(0, 255, 0));
    ui->timeLabel->setAutoFillBackground(true);
    ui->timeLabel->setPalette(palette);
    ui->timeLabel->raise();

    //窗口大小和背景图片
    resize(int(windowWidth),int(windowHeight));
    //QPalette palette(this->palette());
    //palette.setColor(QPalette::Background, Qt::blue);
    //this->setPalette(palette);

    //挡板图片和初始位置
    ui->baffleLabel->setPixmap(QPixmap(":/new/characters/images/characters/bafflePicture.png"));
    ui->baffleLabel->setScaledContents(true);
    ui->baffleLabel->setGeometry(int(baffleX),int(baffleY),int(baffleWidth),int(baffleHeight));

    //球图片和初始位置
    ui->ballLabel->setPixmap(QPixmap(":/new/characters/images/characters/ballPicture.png"));
    ui->ballLabel->setScaledContents(true);
    ui->ballLabel->setGeometry(int(ballX),int(ballY),int(ballRadius*2),int(ballRadius*2));
    ui->ballLabel->raise();
    ui->ballLabel2->setGeometry(int(ballX),int(ballY),int(ballRadius*2),int(ballRadius*2));
    ui->ballLabel2->setPixmap(QPixmap(":/new/characters/images/characters/ballPicture2.png"));
    ui->ballLabel2->setScaledContents(true);
    ui->ballLabel2->raise();
    ui->ballLabel2->hide();

    //球移动事件关联
    connect(ballMoveTimer,SIGNAL(timeout()),this,SLOT(moveBallEvent()));
    //碰撞关联
    connect(collisionTimer,SIGNAL(timeout()),this,SLOT(collisionSituation()));
    //生命值关联
    connect(healthTimer,SIGNAL(timeout()),this,SLOT(healthShow()));
    //通关计时器关联
    connect(clearanceTimer,SIGNAL(timeout()),this,SLOT(clearanceTimerEvent()));
    //特殊砖块启动关联
    connect(specialTimer_1,SIGNAL(timeout()),this,SLOT(special_1()));
    connect(specialTimer_2,SIGNAL(timeout()),this,SLOT(special_2()));
    ui->laserLabel->setGeometry(int(baffleX + baffleWidth/ 2 - laserWidth/2),int(windowHeight*0.11),int(laserWidth),int(baffleY-windowHeight*0.11));
    ui->laserLabel->hide();
    ui->laserLabel->setScaledContents(true);
    //球影子残留定时器
    connect(flashBallTimer,SIGNAL(timeout()),this,SLOT(flashBallEvent()));

    //障碍物初始化|关卡地图初始化
    if(level_now == 1){//关卡1
        obstacleNum = 15;
        limitTime = 60;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;

        obstacleX[0] = windowWidth * 0.18;
        obstacleX[1] = windowWidth * 0.18;
        obstacleX[2] = windowWidth * 0.18;
        obstacleX[3] = windowWidth * 0.18;
        obstacleX[4] = windowWidth * 0.18;
        obstacleY[0] = windowHeight * 0.17;
        obstacleY[1] = obstacleY[0] + obstacleHeight + windowHeight * 0.045;
        obstacleY[2] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 2;
        obstacleY[3] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 3;
        obstacleY[4] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 4;

        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[7] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[8] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[9] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleY[5] = windowHeight * 0.17;
        obstacleY[6] = obstacleY[0] + obstacleHeight + windowHeight * 0.045;
        obstacleY[7] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 2;
        obstacleY[8] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 3;
        obstacleY[9] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 4;

        obstacleX[10] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[11] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[12] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[13] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[14] = windowWidth * 0.82 - obstacleWidth;
        obstacleY[10] = windowHeight * 0.17;
        obstacleY[11] = obstacleY[0] + obstacleHeight + windowHeight * 0.045;
        obstacleY[12] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 2;
        obstacleY[13] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 3;
        obstacleY[14] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 4;
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        };
        //红
        for(int i = 0;i < 4;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 4;i < 8;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 8;i < 13;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 12;i < obstacleNum;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
    };
    if(level_now == 2){//关卡2
        obstacleNum = 25;
        limitTime = 55;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;
        obstacleLabel[20] = ui->obstacleLabel_21;
        obstacleLabel[21] = ui->obstacleLabel_22;
        obstacleLabel[22] = ui->obstacleLabel_23;
        obstacleLabel[23] = ui->obstacleLabel_24;
        obstacleLabel[24] = ui->obstacleLabel_25;

        obstacleX[0] =windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[1] =windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[2] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[3] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[4] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleY[0] = windowHeight * 0.17;
        obstacleY[1] = windowHeight * 0.17+obstacleHeight;
        obstacleY[2] = windowHeight * 0.17+obstacleHeight*2;
        obstacleY[3] = windowHeight * 0.17+obstacleHeight*3;
        obstacleY[4] = windowHeight * 0.17+obstacleHeight*4;
        obstacleY[5] = windowHeight * 0.17+obstacleHeight*5;
        obstacleY[6] = windowHeight * 0.17+obstacleHeight*6;

        obstacleX[7] =  windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[8] =  windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[9] =  windowWidth * 0.5 - obstacleWidth * 1.5;;
        obstacleX[10] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[11] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[12] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[13] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[14] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[15] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[16] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleY[7] = windowHeight * 0.17+obstacleHeight;
        obstacleY[8] = windowHeight * 0.17+obstacleHeight*2;
        obstacleY[9] = windowHeight * 0.17+obstacleHeight*3;
        obstacleY[10] = windowHeight * 0.17+obstacleHeight*4;
        obstacleY[11] = windowHeight * 0.17+obstacleHeight*5;
        obstacleY[12] = windowHeight * 0.17+obstacleHeight;
        obstacleY[13] = windowHeight * 0.17+obstacleHeight*2;
        obstacleY[14] = windowHeight * 0.17+obstacleHeight*3;
        obstacleY[15] = windowHeight * 0.17+obstacleHeight*4;
        obstacleY[16] = windowHeight * 0.17+obstacleHeight*5;

        obstacleX[17] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[18] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[19] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[20] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[21] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[22] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleY[17] = windowHeight * 0.17+obstacleHeight*2;
        obstacleY[18] = windowHeight * 0.17+obstacleHeight*3;
        obstacleY[19] = windowHeight * 0.17+obstacleHeight*4;
        obstacleY[20] = windowHeight * 0.17+obstacleHeight*2;
        obstacleY[21] = windowHeight * 0.17+obstacleHeight*3;
        obstacleY[22] = windowHeight * 0.17+obstacleHeight*4;

        obstacleX[23] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[24] = windowWidth * 0.5 + obstacleWidth*2.5;
        obstacleY[23] = windowHeight * 0.17 + obstacleHeight*3;
        obstacleY[24] = windowHeight * 0.17 + obstacleHeight*3;
        //红
         for(int i = 0;i < 7;i++){
            obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
            obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
            obstacleLabel[i]->setScaledContents(true);
         };
         //黄
         for(int i = 7;i < 17;i++){
            obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
            obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
            obstacleLabel[i]->setScaledContents(true);
         };
         //红
         for(int i = 17;i <23;i++){
            obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
            obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
            obstacleLabel[i]->setScaledContents(true);
         };
         //黄
         for(int i = 23;i <obstacleNum;i++){
            obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
            obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
            obstacleLabel[i]->setScaledContents(true);
         };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }

    };
    if(level_now == 3){//关卡3
        obstacleNum=32;
        limitTime = 50;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;
        obstacleLabel[20] = ui->obstacleLabel_21;
        obstacleLabel[21] = ui->obstacleLabel_22;
        obstacleLabel[22] = ui->obstacleLabel_23;
        obstacleLabel[23] = ui->obstacleLabel_24;
        obstacleLabel[24] = ui->obstacleLabel_25;
        obstacleLabel[25] = ui->obstacleLabel_26;
        obstacleLabel[26] = ui->obstacleLabel_27;
        obstacleLabel[27] = ui->obstacleLabel_28;
        obstacleLabel[28] = ui->obstacleLabel_29;
        obstacleLabel[29] = ui->obstacleLabel_30;
        obstacleLabel[30] = ui->obstacleLabel_31;
        obstacleLabel[31] = ui->obstacleLabel_32;

        obstacleX[0] =windowWidth * 0.5 - obstacleWidth ;
        obstacleX[1] =windowWidth * 0.5 - obstacleWidth ;
        obstacleX[2] = windowWidth * 0.5 ;
        obstacleX[3] = windowWidth * 0.5 ;
        obstacleY[0] = windowHeight * 0.17+obstacleHeight*3;
        obstacleY[1] = windowHeight * 0.17+obstacleHeight*4;
        obstacleY[2] = windowHeight * 0.17+obstacleHeight*4;
        obstacleY[3] = windowHeight * 0.17+obstacleHeight*5;

        obstacleX[4] =windowWidth * 0.5 - obstacleWidth*2;
        obstacleX[5] =windowWidth * 0.5 - obstacleWidth*2;
        obstacleX[6] =windowWidth * 0.5 - obstacleWidth*2;
        obstacleX[7] =windowWidth * 0.5 - obstacleWidth*2;
        obstacleX[8] =windowWidth * 0.5 + obstacleWidth;
        obstacleX[9] =windowWidth * 0.5 + obstacleWidth;
        obstacleX[10] =windowWidth * 0.5 + obstacleWidth;
        obstacleX[11] =windowWidth * 0.5 + obstacleWidth;
        obstacleY[4] = windowHeight * 0.17 + obstacleHeight*1;
        obstacleY[5] = windowHeight * 0.17 + obstacleHeight*2;
        obstacleY[6] = windowHeight * 0.17 + obstacleHeight*5;
        obstacleY[7] = windowHeight * 0.17 + obstacleHeight*6;
        obstacleY[8] = windowHeight * 0.17 + obstacleHeight*2;
        obstacleY[9] = windowHeight * 0.17 + obstacleHeight*3;
        obstacleY[10] = windowHeight * 0.17 + obstacleHeight*6;
        obstacleY[11] = windowHeight * 0.17 + obstacleHeight*7;

        obstacleX[12] =windowWidth * 0.5 - obstacleWidth*3;
        obstacleX[13] =windowWidth * 0.5 - obstacleWidth*3;
        obstacleX[14] =windowWidth * 0.5 - obstacleWidth*3;
        obstacleX[15] =windowWidth * 0.5 - obstacleWidth*3;
        obstacleX[16] =windowWidth * 0.5 + obstacleWidth*2;
        obstacleX[17] =windowWidth * 0.5 + obstacleWidth*2;
        obstacleX[18] =windowWidth * 0.5 + obstacleWidth*2;
        obstacleX[19] =windowWidth * 0.5 + obstacleWidth*2;
        obstacleY[12] = windowHeight * 0.17;
        obstacleY[13] = windowHeight * 0.17 + obstacleHeight*3;
        obstacleY[14] = windowHeight * 0.17 + obstacleHeight*4;
        obstacleY[15] = windowHeight * 0.17 + obstacleHeight*7;
        obstacleY[16] = windowHeight * 0.17 + obstacleHeight*1;
        obstacleY[17] = windowHeight * 0.17 + obstacleHeight*4;
        obstacleY[18] = windowHeight * 0.17 + obstacleHeight*5;
        obstacleY[19] = windowHeight * 0.17 + obstacleHeight*8;

        obstacleX[20] =windowWidth * 0.5 - obstacleWidth*4;
        obstacleX[21] =windowWidth * 0.5 - obstacleWidth*4;
        obstacleX[22] =windowWidth * 0.5 - obstacleWidth*4;
        obstacleX[23] =windowWidth * 0.5 - obstacleWidth*4;
        obstacleX[24] =windowWidth * 0.5 + obstacleWidth*3;
        obstacleX[25] =windowWidth * 0.5 + obstacleWidth*3;
        obstacleX[26] =windowWidth * 0.5 + obstacleWidth*3;
        obstacleX[27] =windowWidth * 0.5 + obstacleWidth*3;
        obstacleY[20] = windowHeight * 0.17 + obstacleHeight*1;
        obstacleY[21] = windowHeight * 0.17 + obstacleHeight*2;
        obstacleY[22] = windowHeight * 0.17 + obstacleHeight*5;
        obstacleY[23] = windowHeight * 0.17 + obstacleHeight*6;
        obstacleY[24] = windowHeight * 0.17 + obstacleHeight*2;
        obstacleY[25] = windowHeight * 0.17 + obstacleHeight*3;
        obstacleY[26] = windowHeight * 0.17 + obstacleHeight*6;
        obstacleY[27] = windowHeight * 0.17 + obstacleHeight*7;

        obstacleX[28] =windowWidth * 0.5 - obstacleWidth*5;
        obstacleX[29] =windowWidth * 0.5 - obstacleWidth*5;
        obstacleX[30] =windowWidth * 0.5 + obstacleWidth*4;
        obstacleX[31] =windowWidth * 0.5 + obstacleWidth*4;
        obstacleY[28] = windowHeight * 0.17 + obstacleHeight*3;
        obstacleY[29] = windowHeight * 0.17 + obstacleHeight*4;
        obstacleY[30] = windowHeight * 0.17 + obstacleHeight*4;
        obstacleY[31] = windowHeight * 0.17 + obstacleHeight*5;
        //蓝
        for(int i = 0;i <2;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 2;i <4;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 4;i <8;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 8;i <16;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 16;i <24;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 24;i <28;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 28;i <30;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 30;i <obstacleNum;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    };
    if(level_now == 4){//关卡4
        obstacleNum=25;
        limitTime = 45;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;
        obstacleLabel[20] = ui->obstacleLabel_21;
        obstacleLabel[21] = ui->obstacleLabel_22;
        obstacleLabel[22] = ui->obstacleLabel_23;
        obstacleLabel[23] = ui->obstacleLabel_24;
        obstacleLabel[24] = ui->obstacleLabel_25;

        obstacleX[0] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[1] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[2] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[3] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[4] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleY[0] = windowHeight * 0.17;
        obstacleY[1] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[2] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[3] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[4] = windowHeight * 0.17 + obstacleHeight * 8;

        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[7] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[8] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[9] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[10] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[11] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[12] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleY[5] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[6] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[7] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[8] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[9] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[10] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[11] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[12] = windowHeight * 0.17 + obstacleHeight * 7;

        obstacleX[13] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[14] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[15] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[16] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[17] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[18] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleY[13] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[14] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[15] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[16] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[17] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[18] = windowHeight * 0.17 + obstacleHeight * 6;

        obstacleX[19] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[20] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[21] = windowWidth * 0.5 + obstacleWidth * 2.5;
        obstacleX[22] = windowWidth * 0.5 + obstacleWidth * 2.5;
        obstacleY[19] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[20] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[21] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[22] = windowHeight * 0.17 + obstacleHeight * 5;

        obstacleX[23] = windowWidth * 0.5 - obstacleWidth * 4.5;
        obstacleX[24] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleY[23] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[24] = windowHeight * 0.17 + obstacleHeight * 4;
        //绿
        for(int i = 0;i < 5;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 5;i < 13;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 13;i < 19;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 19;i < 23;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 23;i < obstacleNum;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    };
    if(level_now == 5){//关卡5
        obstacleNum=20;
        limitTime = 40;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;

        obstacleX[0] = windowWidth * 0.5  - obstacleWidth;
        obstacleX[1] = windowWidth * 0.5  - obstacleWidth;
        obstacleX[2] = windowWidth * 0.5;
        obstacleX[3] = windowWidth * 0.5;
        obstacleY[0] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[1] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[2] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[3] = windowHeight * 0.17 + obstacleHeight * 4;

        obstacleX[4] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[6] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[7] = windowWidth * 0.5 + obstacleWidth;
        obstacleY[4] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[5] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[6] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[7] = windowHeight * 0.17 + obstacleHeight * 5;

        obstacleX[8] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[9] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[10] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[11] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleY[8] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[9] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[10] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[11] = windowHeight * 0.17 + obstacleHeight * 6;

        obstacleX[12] = windowWidth * 0.5 - obstacleWidth * 4;
        obstacleX[13] = windowWidth * 0.5 - obstacleWidth * 4;
        obstacleX[14] = windowWidth * 0.5 + obstacleWidth * 3;
        obstacleX[15] = windowWidth * 0.5 + obstacleWidth * 3;
        obstacleY[12] = windowHeight * 0.17 ;
        obstacleY[13] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[14] = windowHeight * 0.17 ;
        obstacleY[15] = windowHeight * 0.17 + obstacleHeight * 7;

        obstacleX[16] = windowWidth * 0.5  - obstacleWidth * 5;
        obstacleX[17] = windowWidth * 0.5  - obstacleWidth * 5;
        obstacleX[18] = windowWidth * 0.5 + obstacleWidth * 4;
        obstacleX[19] = windowWidth * 0.5 + obstacleWidth * 4;
        obstacleY[16] = windowHeight * 0.17 ;
        obstacleY[17] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[18] = windowHeight * 0.17 ;
        obstacleY[19] = windowHeight * 0.17 + obstacleHeight * 7;
        //绿
        for(int i = 0;i < 1;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //褐
        for(int i = 1;i < 3;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 3;i < 5;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //褐
        for(int i =5;i < 7;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 7;i < 9;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //褐
        for(int i = 9;i < 11;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 11;i < 13;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //褐
        for(int i = 13;i < 15;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 15;i < 17;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //褐
        for(int i = 17;i < 19;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 19;i < obstacleNum ;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    };
    if(level_now == 6){//关卡6
        obstacleNum=48;
        limitTime = 35;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;
        obstacleLabel[20] = ui->obstacleLabel_21;
        obstacleLabel[21] = ui->obstacleLabel_22;
        obstacleLabel[22] = ui->obstacleLabel_23;
        obstacleLabel[23] = ui->obstacleLabel_24;
        obstacleLabel[24] = ui->obstacleLabel_25;
        obstacleLabel[25] = ui->obstacleLabel_26;
        obstacleLabel[26] = ui->obstacleLabel_27;
        obstacleLabel[27] = ui->obstacleLabel_28;
        obstacleLabel[28] = ui->obstacleLabel_29;
        obstacleLabel[29] = ui->obstacleLabel_30;
        obstacleLabel[30] = ui->obstacleLabel_31;
        obstacleLabel[31] = ui->obstacleLabel_32;
        obstacleLabel[32] = ui->obstacleLabel_33;
        obstacleLabel[33] = ui->obstacleLabel_34;
        obstacleLabel[34] = ui->obstacleLabel_35;
        obstacleLabel[35] = ui->obstacleLabel_36;
        obstacleLabel[36] = ui->obstacleLabel_37;
        obstacleLabel[37] = ui->obstacleLabel_38;
        obstacleLabel[38] = ui->obstacleLabel_39;
        obstacleLabel[39] = ui->obstacleLabel_40;
        obstacleLabel[40] = ui->obstacleLabel_41;
        obstacleLabel[41] = ui->obstacleLabel_42;
        obstacleLabel[42] = ui->obstacleLabel_43;
        obstacleLabel[43] = ui->obstacleLabel_44;
        obstacleLabel[44] = ui->obstacleLabel_45;
        obstacleLabel[45] = ui->obstacleLabel_46;
        obstacleLabel[46] = ui->obstacleLabel_47;
        obstacleLabel[47] = ui->obstacleLabel_48;

        obstacleX[0] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[1] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[2] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[3] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[4] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[5] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[6] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[7] = windowWidth * 0.5 - obstacleWidth;
        obstacleX[8] = windowWidth * 0.5 ;
        obstacleX[9] = windowWidth * 0.5 ;
        obstacleX[10] = windowWidth * 0.5 ;
        obstacleX[11] = windowWidth * 0.5 ;
        obstacleX[12] = windowWidth * 0.5 ;
        obstacleX[13] = windowWidth * 0.5 ;
        obstacleX[14] = windowWidth * 0.5 ;
        obstacleX[15] = windowWidth * 0.5 ;
        obstacleY[0] = windowHeight * 0.17;
        obstacleY[1] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[2] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[3] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[4] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[5] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[6] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[7] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[8] = windowHeight * 0.17;
        obstacleY[9] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[10] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[11] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[12] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[13] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[14] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[15] = windowHeight * 0.17 + obstacleHeight * 7;

        obstacleX[16] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[17] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[18] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[19] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[20] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[21] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[22] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[23] = windowWidth * 0.5 - obstacleWidth * 2;
        obstacleX[24] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[25] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[26] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[27] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[28] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[29] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[30] = windowWidth * 0.5 + obstacleWidth;
        obstacleX[31] = windowWidth * 0.5 + obstacleWidth;
        obstacleY[16] = windowHeight * 0.17;
        obstacleY[17] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[18] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[19] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[20] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[21] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[22] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[23] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[24] = windowHeight * 0.17;
        obstacleY[25] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[26] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[27] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[28] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[29] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[30] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[31] = windowHeight * 0.17 + obstacleHeight * 7;

        obstacleX[32] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[33] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[34] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[35] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[36] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[37] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[38] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[39] = windowWidth * 0.5 - obstacleWidth * 3;
        obstacleX[40] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[41] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[42] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[43] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[44] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[45] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[46] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleX[47] = windowWidth * 0.5 + obstacleWidth * 2;
        obstacleY[32] = windowHeight * 0.17;
        obstacleY[33] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[34] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[35] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[36] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[37] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[38] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[39] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[40] = windowHeight * 0.17;
        obstacleY[41] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[42] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[43] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[44] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[45] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[46] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[47] = windowHeight * 0.17 + obstacleHeight * 7;
        //黄
        for(int i = 0;i < 6;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/whole/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //红
        for(int i = 6;i < 7;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 7;i < 14;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //红
        for(int i = 14;i < 15;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 15;i < 18;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 18;i < 19;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 19;i < 22;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //红
        for(int i = 22;i < 23;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 23;i < 26;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 26;i < 27;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 27;i < 30;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //红
        for(int i = 30;i < 31;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
         //黄
        for(int i = 31;i < 37;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //红
        for(int i = 37;i < 38;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
       // 黄
        for(int i = 38;i < 45;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //红
        for(int i = 45;i < 46;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 46;i < obstacleNum;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        }
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    };
    if(level_now == 7){//关卡7
        obstacleNum=40;
        limitTime = 35;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
         obstacleLabel[0] = ui->obstacleLabel_1;
         obstacleLabel[1] = ui->obstacleLabel_2;
         obstacleLabel[2] = ui->obstacleLabel_3;
         obstacleLabel[3] = ui->obstacleLabel_4;
         obstacleLabel[4] = ui->obstacleLabel_5;
         obstacleLabel[5] = ui->obstacleLabel_6;
         obstacleLabel[6] = ui->obstacleLabel_7;
         obstacleLabel[7] = ui->obstacleLabel_8;
         obstacleLabel[8] = ui->obstacleLabel_9;
         obstacleLabel[9] = ui->obstacleLabel_10;
         obstacleLabel[10] = ui->obstacleLabel_11;
         obstacleLabel[11] = ui->obstacleLabel_12;
         obstacleLabel[12] = ui->obstacleLabel_13;
         obstacleLabel[13] = ui->obstacleLabel_14;
         obstacleLabel[14] = ui->obstacleLabel_15;
         obstacleLabel[15] = ui->obstacleLabel_16;
         obstacleLabel[16] = ui->obstacleLabel_17;
         obstacleLabel[17] = ui->obstacleLabel_18;
         obstacleLabel[18] = ui->obstacleLabel_19;
         obstacleLabel[19] = ui->obstacleLabel_20;
         obstacleLabel[20] = ui->obstacleLabel_21;
         obstacleLabel[21] = ui->obstacleLabel_22;
         obstacleLabel[22] = ui->obstacleLabel_23;
         obstacleLabel[23] = ui->obstacleLabel_24;
         obstacleLabel[24] = ui->obstacleLabel_25;
         obstacleLabel[25] = ui->obstacleLabel_26;
         obstacleLabel[26] = ui->obstacleLabel_27;
         obstacleLabel[27] = ui->obstacleLabel_28;
         obstacleLabel[28] = ui->obstacleLabel_29;
         obstacleLabel[29] = ui->obstacleLabel_30;
         obstacleLabel[30] = ui->obstacleLabel_31;
         obstacleLabel[31] = ui->obstacleLabel_32;
         obstacleLabel[32] = ui->obstacleLabel_33;
         obstacleLabel[33] = ui->obstacleLabel_34;
         obstacleLabel[34] = ui->obstacleLabel_35;
         obstacleLabel[35] = ui->obstacleLabel_36;
         obstacleLabel[36] = ui->obstacleLabel_37;
         obstacleLabel[37] = ui->obstacleLabel_38;
         obstacleLabel[38] = ui->obstacleLabel_39;
         obstacleLabel[39] = ui->obstacleLabel_40;

         obstacleX[0] = windowWidth * 0.5 - obstacleWidth * 2;
         obstacleX[1] = windowWidth * 0.5 - obstacleWidth * 1.5;
         obstacleX[2] = windowWidth * 0.5 - obstacleWidth * 1.5;
         obstacleX[3] = windowWidth * 0.5 - obstacleWidth * 1.5;
         obstacleX[4] = windowWidth * 0.5 - obstacleWidth * 1.5;
         obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 1.5;
         obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 2;
         obstacleX[7] = windowWidth * 0.5 - obstacleWidth * 2;
         obstacleX[8] = windowWidth * 0.5 - obstacleWidth * 2;
         obstacleX[9] = windowWidth * 0.5 - obstacleWidth * 2;
         obstacleX[10] = windowWidth * 0.5 + obstacleWidth;
         obstacleX[11] = windowWidth * 0.5 + obstacleWidth * 0.5;
         obstacleX[12] = windowWidth * 0.5 + obstacleWidth * 0.5;
         obstacleX[13] = windowWidth * 0.5 + obstacleWidth * 0.5;
         obstacleX[14] = windowWidth * 0.5 + obstacleWidth * 0.5;
         obstacleX[15] = windowWidth * 0.5 + obstacleWidth * 0.5;
         obstacleX[16] = windowWidth * 0.5 + obstacleWidth;
         obstacleX[17] = windowWidth * 0.5 + obstacleWidth;
         obstacleX[18] = windowWidth * 0.5 + obstacleWidth;
         obstacleX[19] = windowWidth * 0.5 + obstacleWidth;
         obstacleY[0] = windowHeight * 0.17;
         obstacleY[1] = windowHeight * 0.17 + obstacleHeight * 1;
         obstacleY[2] = windowHeight * 0.17 + obstacleHeight * 2;
         obstacleY[3] = windowHeight * 0.17 + obstacleHeight * 3;
         obstacleY[4] = windowHeight * 0.17 + obstacleHeight * 4;
         obstacleY[5] = windowHeight * 0.17 + obstacleHeight * 5;
         obstacleY[6] = windowHeight * 0.17 + obstacleHeight * 6;
         obstacleY[7] = windowHeight * 0.17 + obstacleHeight * 7;
         obstacleY[8] = windowHeight * 0.17 + obstacleHeight * 8;
         obstacleY[9] = windowHeight * 0.17 + obstacleHeight * 9;
         obstacleY[10] = windowHeight * 0.17;
         obstacleY[11] = windowHeight * 0.17 + obstacleHeight * 1;
         obstacleY[12] = windowHeight * 0.17 + obstacleHeight * 2;
         obstacleY[13] = windowHeight * 0.17 + obstacleHeight * 3;
         obstacleY[14] = windowHeight * 0.17 + obstacleHeight * 4;
         obstacleY[15] = windowHeight * 0.17 + obstacleHeight * 5;
         obstacleY[16] = windowHeight * 0.17 + obstacleHeight * 6;
         obstacleY[17] = windowHeight * 0.17 + obstacleHeight * 7;
         obstacleY[18] = windowHeight * 0.17 + obstacleHeight * 8;
         obstacleY[19] = windowHeight * 0.17 + obstacleHeight * 9;

         obstacleX[20] = windowWidth * 0.5 - obstacleWidth * 3.5;
         obstacleX[21] = windowWidth * 0.5 - obstacleWidth * 4;
         obstacleX[22] = windowWidth * 0.5 - obstacleWidth * 4;
         obstacleX[23] = windowWidth * 0.5 - obstacleWidth * 4;
         obstacleX[24] = windowWidth * 0.5 - obstacleWidth * 4;
         obstacleX[25] = windowWidth * 0.5 - obstacleWidth * 4;
         obstacleX[26] = windowWidth * 0.5 - obstacleWidth * 3.5;
         obstacleX[27] = windowWidth * 0.5 - obstacleWidth * 3.5;
         obstacleX[28] = windowWidth * 0.5 - obstacleWidth * 3.5;
         obstacleX[29] = windowWidth * 0.5 - obstacleWidth * 3.5;
         obstacleX[30] = windowWidth * 0.5 + obstacleWidth * 2.5;
         obstacleX[31] = windowWidth * 0.5 + obstacleWidth * 3;
         obstacleX[32] = windowWidth * 0.5 + obstacleWidth * 3;
         obstacleX[33] = windowWidth * 0.5 + obstacleWidth * 3;
         obstacleX[34] = windowWidth * 0.5 + obstacleWidth * 3;
         obstacleX[35] = windowWidth * 0.5 + obstacleWidth * 3;
         obstacleX[36] = windowWidth * 0.5 + obstacleWidth * 2.5;
         obstacleX[37] = windowWidth * 0.5 + obstacleWidth * 2.5;
         obstacleX[38] = windowWidth * 0.5 + obstacleWidth * 2.5;
         obstacleX[39] = windowWidth * 0.5 + obstacleWidth * 2.5;
         obstacleY[20] = windowHeight * 0.17;
         obstacleY[21] = windowHeight * 0.17 + obstacleHeight * 1;
         obstacleY[22] = windowHeight * 0.17 + obstacleHeight * 2;
         obstacleY[23] = windowHeight * 0.17 + obstacleHeight * 3;
         obstacleY[24] = windowHeight * 0.17 + obstacleHeight * 4;
         obstacleY[25] = windowHeight * 0.17 + obstacleHeight * 5;
         obstacleY[26] = windowHeight * 0.17 + obstacleHeight * 6;
         obstacleY[27] = windowHeight * 0.17 + obstacleHeight * 7;
         obstacleY[28] = windowHeight * 0.17 + obstacleHeight * 8;
         obstacleY[29] = windowHeight * 0.17 + obstacleHeight * 9;
         obstacleY[30] = windowHeight * 0.17;
         obstacleY[31] = windowHeight * 0.17 + obstacleHeight * 1;
         obstacleY[32] = windowHeight * 0.17 + obstacleHeight * 2;
         obstacleY[33] = windowHeight * 0.17 + obstacleHeight * 3;
         obstacleY[34] = windowHeight * 0.17 + obstacleHeight * 4;
         obstacleY[35] = windowHeight * 0.17 + obstacleHeight * 5;
         obstacleY[36] = windowHeight * 0.17 + obstacleHeight * 6;
         obstacleY[37] = windowHeight * 0.17 + obstacleHeight * 7;
         obstacleY[38] = windowHeight * 0.17 + obstacleHeight * 8;
         obstacleY[39] = windowHeight * 0.17 + obstacleHeight * 9;

         //褐
         for(int i = 0;i < 10;i++){
             obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
             obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
             obstacleLabel[i]->setScaledContents(true);
            };
         //黄
         for(int i = 10;i < 20;i++){
             obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
             obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
             obstacleLabel[i]->setScaledContents(true);
            };
         //褐
         for(int i = 20;i < 30;i++){
             obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
             obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
             obstacleLabel[i]->setScaledContents(true);
            };
         //黄
         for(int i = 30;i <obstacleNum ;i++){
             obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
             obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
             obstacleLabel[i]->setScaledContents(true);
            };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    };
    if(level_now == 8){//关卡8
        obstacleNum=30;
        limitTime = 35;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;
        obstacleLabel[20] = ui->obstacleLabel_21;
        obstacleLabel[21] = ui->obstacleLabel_22;
        obstacleLabel[22] = ui->obstacleLabel_23;
        obstacleLabel[23] = ui->obstacleLabel_24;
        obstacleLabel[24] = ui->obstacleLabel_25;
        obstacleLabel[25] = ui->obstacleLabel_26;
        obstacleLabel[26] = ui->obstacleLabel_27;
        obstacleLabel[27] = ui->obstacleLabel_28;
        obstacleLabel[28] = ui->obstacleLabel_29;
        obstacleLabel[29] = ui->obstacleLabel_30;

        obstacleX[0] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[1] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[2] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[3] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleY[0] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[1] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[2] = windowHeight * 0.17 + obstacleHeight * 8;
        obstacleY[3] = windowHeight * 0.17 + obstacleHeight * 10;

        obstacleX[4] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 1.5;
        obstacleX[7] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[8] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleX[9] = windowWidth * 0.5 + obstacleWidth * 0.5;
        obstacleY[4] = windowHeight * 0.17;
        obstacleY[5] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[6] = windowHeight * 0.17 + obstacleHeight * 9;
        obstacleY[7] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[8] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[9] = windowHeight * 0.17 + obstacleHeight *11;

        obstacleX[10] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[11] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[12] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[13] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[14] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[15] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleY[10] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[11] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[12] = windowHeight * 0.17 + obstacleHeight * 11;
        obstacleY[13] = windowHeight * 0.17;
        obstacleY[14] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[15] = windowHeight * 0.17 + obstacleHeight * 9;

        obstacleX[16] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[17] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[18] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[19] = windowWidth * 0.5 - obstacleWidth * 3.5;
        obstacleX[20] = windowWidth * 0.5 + obstacleWidth * 2.5;
        obstacleX[21] = windowWidth * 0.5 + obstacleWidth * 2.5;
        obstacleX[22] = windowWidth * 0.5 + obstacleWidth * 2.5;
        obstacleX[23] = windowWidth * 0.5 + obstacleWidth * 2.5;
        obstacleY[16] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[17] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[18] = windowHeight * 0.17 + obstacleHeight * 8;
        obstacleY[19] = windowHeight * 0.17 + obstacleHeight * 10;
        obstacleY[20] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[21] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[22] = windowHeight * 0.17 + obstacleHeight * 8;
        obstacleY[23] = windowHeight * 0.17 + obstacleHeight * 10;

        obstacleX[24] = windowWidth * 0.5 - obstacleWidth * 4.5;
        obstacleX[25] = windowWidth * 0.5 - obstacleWidth * 4.5;
        obstacleX[26] = windowWidth * 0.5 - obstacleWidth * 4.5;
        obstacleX[27] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[28] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[29] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleY[24] = windowHeight * 0.17;
        obstacleY[25] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[26] = windowHeight * 0.17 + obstacleHeight * 9;
        obstacleY[27] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[28] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[29] = windowHeight * 0.17 + obstacleHeight * 11;
        for(int i = 0;i < obstacleNum;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    };
    if(level_now == 9){//关卡9
        obstacleNum = 30;
        limitTime = 30;
        ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
        obstacleLabel[0] = ui->obstacleLabel_1;
        obstacleLabel[1] = ui->obstacleLabel_2;
        obstacleLabel[2] = ui->obstacleLabel_3;
        obstacleLabel[3] = ui->obstacleLabel_4;
        obstacleLabel[4] = ui->obstacleLabel_5;
        obstacleLabel[5] = ui->obstacleLabel_6;
        obstacleLabel[6] = ui->obstacleLabel_7;
        obstacleLabel[7] = ui->obstacleLabel_8;
        obstacleLabel[8] = ui->obstacleLabel_9;
        obstacleLabel[9] = ui->obstacleLabel_10;
        obstacleLabel[10] = ui->obstacleLabel_11;
        obstacleLabel[11] = ui->obstacleLabel_12;
        obstacleLabel[12] = ui->obstacleLabel_13;
        obstacleLabel[13] = ui->obstacleLabel_14;
        obstacleLabel[14] = ui->obstacleLabel_15;
        obstacleLabel[15] = ui->obstacleLabel_16;
        obstacleLabel[16] = ui->obstacleLabel_17;
        obstacleLabel[17] = ui->obstacleLabel_18;
        obstacleLabel[18] = ui->obstacleLabel_19;
        obstacleLabel[19] = ui->obstacleLabel_20;
        obstacleLabel[20] = ui->obstacleLabel_21;
        obstacleLabel[21] = ui->obstacleLabel_22;
        obstacleLabel[22] = ui->obstacleLabel_23;
        obstacleLabel[23] = ui->obstacleLabel_24;
        obstacleLabel[24] = ui->obstacleLabel_25;
        obstacleLabel[25] = ui->obstacleLabel_26;
        obstacleLabel[26] = ui->obstacleLabel_27;
        obstacleLabel[27] = ui->obstacleLabel_28;
        obstacleLabel[28] = ui->obstacleLabel_29;
        obstacleLabel[29] = ui->obstacleLabel_30;

        obstacleX[0] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[1] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[2] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[3] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[4] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleY[0] = windowHeight * 0.17;
        obstacleY[1] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[2] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[3] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[4] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[5] = windowHeight * 0.17 + obstacleHeight * 5;

        obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[7] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[8] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleX[9] = windowWidth * 0.5 - obstacleWidth * 2.5;
        obstacleY[6] = windowHeight * 0.17;
        obstacleY[7] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[8] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[9] = windowHeight * 0.17 + obstacleHeight * 3;

        obstacleX[10] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[11] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[12] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[13] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[14] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[15] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[16] = windowWidth * 0.5 + obstacleWidth * 1.5;
        obstacleX[17] = windowWidth * 0.5 + obstacleWidth * 1.5;

        obstacleY[10] = windowHeight * 0.17;
        obstacleY[11] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[12] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[13] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[14] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[15] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[16] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[17] = windowHeight * 0.17 + obstacleHeight * 7;

        obstacleX[18] = windowWidth * 0.5 - obstacleWidth * 4.5;
        obstacleX[19] = windowWidth * 0.5 - obstacleWidth * 4.5;
        obstacleY[18] = windowHeight * 0.17;
        obstacleY[19] = windowHeight * 0.17 + obstacleHeight * 1;

        obstacleX[20] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[21] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[22] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[23] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[24] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[25] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[26] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[27] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[28] = windowWidth * 0.5 + obstacleWidth * 3.5;
        obstacleX[29] = windowWidth * 0.5 + obstacleWidth * 3.5;

        obstacleY[20] = windowHeight * 0.17;
        obstacleY[21] = windowHeight * 0.17 + obstacleHeight * 1;
        obstacleY[22] = windowHeight * 0.17 + obstacleHeight * 2;
        obstacleY[23] = windowHeight * 0.17 + obstacleHeight * 3;
        obstacleY[24] = windowHeight * 0.17 + obstacleHeight * 4;
        obstacleY[25] = windowHeight * 0.17 + obstacleHeight * 5;
        obstacleY[26] = windowHeight * 0.17 + obstacleHeight * 6;
        obstacleY[27] = windowHeight * 0.17 + obstacleHeight * 7;
        obstacleY[28] = windowHeight * 0.17 + obstacleHeight * 8;
        obstacleY[29] = windowHeight * 0.17 + obstacleHeight * 9;

        //红
        for(int i = 0;i <6;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle1.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //黄
        for(int i = 6;i <10;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle2.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //蓝
        for(int i = 10;i <18;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle3.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //绿
        for(int i = 18;i <20;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle4.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        //褐
        for(int i = 20;i <obstacleNum;i++){
           obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
           obstacleLabel[i]->setPixmap(QPixmap(":/new/characters/images/characters/obstacle5.png"));
           obstacleLabel[i]->setScaledContents(true);
        };
        for(int i = obstacleNum; i<100;i++){
           obstacleDestoryed[i] = true;
        }
    }
    for(int i = 0;i<obstacleNum;i++){
         obstacleLabel[i]->raise();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

//窗体自适应事件
void MainWindow::resizeEvent(QResizeEvent*){
    double d_scale_x = this->width() * 1.0 / windowWidth;
    double d_scale_y = this->height() * 1.0 / windowHeight;
    //挡板部分
    baffleY = windowHeight * 7.0 * d_scale_y / 8;
    baffleWidth = windowWidth * 11.0  * d_scale_x/ 30;
    baffleHeight = windowHeight * 1.0  * d_scale_y/ 32;
    ui->baffleLabel->setGeometry(int(baffleX),int(baffleY),int(baffleWidth),int(baffleHeight));
    windowWidth = this->width();
    windowHeight = this->height();
    //状态栏部分
    ui->stateLabel->setGeometry(0,0,int(windowWidth), int(windowHeight*0.11));
    ui->homeButton->setGeometry(int(windowWidth*0.85),int(windowHeight*0.02),int(windowWidth*0.1),int(windowHeight*0.06));
    ui->intervalButton->setGeometry(int(windowWidth*0.75),int(windowHeight*0.02),int(windowWidth*0.1),int(windowHeight*0.06));
    ui->healthLabel->setGeometry(int(windowWidth*0.05),int(windowHeight * 0.02),int(windowWidth*0.25),int(windowHeight*0.06));
    ui->timeLabel->setGeometry(int(windowWidth*0.35),int(windowHeight*0.02),int(windowHeight*0.25),int(windowHeight*0.06));
    //小球部分
    if(launchBall<=1){
        ballRadius =  windowWidth * 0.015;
        ballX =  baffleX + baffleWidth / 2 - ballRadius;
        ballY = baffleY - ballRadius * 2;
        ui->ballLabel->setGeometry(int(ballX),int(ballY),int(ballRadius*2),int(ballRadius*2));
    }
    //障碍物部分//待完善
    if(level_now == 1){//关卡1
        obstacleWidth = windowWidth * 0.12;
        obstacleHeight = obstacleWidth * 0.46;
        obstacleX[0] = windowWidth * 0.18;
        obstacleX[1] = windowWidth * 0.18;
        obstacleX[2] = windowWidth * 0.18;
        obstacleX[3] = windowWidth * 0.18;
        obstacleX[4] = windowWidth * 0.18;
        obstacleY[0] = windowHeight * 0.17;
        obstacleY[1] = obstacleY[0] + obstacleHeight + windowHeight * 0.045;
        obstacleY[2] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 2;
        obstacleY[3] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 3;
        obstacleY[4] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 4;
        obstacleX[5] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[6] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[7] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[8] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleX[9] = windowWidth * 0.5 - obstacleWidth * 0.5;
        obstacleY[5] = windowHeight * 0.17;
        obstacleY[6] = obstacleY[0] + obstacleHeight + windowHeight * 0.045;
        obstacleY[7] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 2;
        obstacleY[8] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 3;
        obstacleY[9] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 4;

        obstacleX[10] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[11] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[12] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[13] = windowWidth * 0.82 - obstacleWidth;
        obstacleX[14] = windowWidth * 0.82 - obstacleWidth;
        obstacleY[10] = windowHeight * 0.17;
        obstacleY[11] = obstacleY[0] + obstacleHeight + windowHeight * 0.045;
        obstacleY[12] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 2;
        obstacleY[13] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 3;
        obstacleY[14] = obstacleY[0] + (obstacleHeight + windowHeight * 0.045) * 4;
        for(int i = 0;i < obstacleNum;i++){
            if(!obstacleDestoryed[i]){
                obstacleLabel[i]->setGeometry(int(obstacleX[i]),int(obstacleY[i]),int(obstacleWidth),int(obstacleHeight));
            }
        }
    };
    if(level_now == 2){//关卡2
    };
    if(level_now == 3){//关卡2
    };
    if(level_now == 4){//关卡2
    };
    if(level_now == 5){//关卡2
    };
    if(level_now == 6){//关卡2
    };
    if(level_now == 7){//关卡2
    };
    if(level_now == 8){//关卡2
    };
    if(level_now == 9){//关卡2
    };
};

//鼠标控制挡板移动事件
void MainWindow::mouseMoveEvent(QMouseEvent *m){
    ui->centralWidget->setMouseTracking(true);
    setMouseTracking(true);//鼠标强制追踪开启
    QPixmapCache::clear();
    QCursor my(QPixmap(":/new/characters/images/icons/mouseOnBaffle.png"));
    ui->baffleLabel->setMouseTracking(true);
    ui->ballLabel->setMouseTracking(true);
    QApplication::setOverrideCursor(my);
    int x = m->pos().x();
    if(m->pos().x() > baffleWidth / 2 && m->pos().x()+baffleWidth/2 < windowWidth){
        baffleRecordX = baffleX;
        baffleX = x-baffleWidth / 2;
        ui->baffleLabel->move(int(baffleX),int(baffleY));
        if(launchBall<=1){
            ballX = baffleX + baffleWidth / 2 - ballRadius;
            ballY = baffleY - ballRadius * 2;
            ui->ballLabel->move(int(ballX),int(ballY));
        }
    }
    if(m->pos().x() < baffleWidth / 2){
        baffleRecordX = baffleX;
        baffleX = 0;
        ui->baffleLabel->move(int(baffleX),int(baffleY));
        if(launchBall<=1){
            ballX = baffleX + baffleWidth / 2 - ballRadius;
            ballY = baffleY - ballRadius * 2;
            ui->ballLabel->move(int(ballX),int(ballY));
        }
    }
    if(m->pos().x() +baffleWidth/2 > windowWidth){
        baffleRecordX = baffleX;
        baffleX = windowWidth - baffleWidth;
        ui->baffleLabel->move(int(baffleX),int(baffleY));
        if(launchBall<=1){
            ballX =  baffleX + baffleWidth / 2 - ballRadius;
            ballY = baffleY - ballRadius * 2;
            ui->ballLabel->move(int(ballX),int(ballY));
        }
    }
    ui->laserLabel->move(int(baffleX + baffleWidth / 2 - laserWidth / 2),int(windowHeight*0.11));
}

//发射小球
void MainWindow::mousePressEvent(QMouseEvent *){
    if(launchBall <= 1){
        launchBall += 1;
    }
    if(launchBall==2){
        if((baffleX - baffleRecordX) >= limitX){
            vx = limitX;
        }
        if((baffleX - baffleRecordX) <= -limitX){
            vx = -limitX;
        }
        else{
            vx = (baffleX - baffleRecordX);
        }
        launchBall +=1;
        ballX = ballX + vx;
        ballY = ballY + vy;
        ui->ballLabel->move(int(ballX),int(ballY));
        ballMoveTimer->start(ballMoveTimerFPS);
        collisionTimer->start(collisionTimerFPS);
        clearanceTimer->start(clearanceTimerFPS);
        healthTimer->start(10);
    }
}

//小球移动事件
void MainWindow::moveBallEvent(){
    if(int(ballX + vx) < 0){
        ui->ballLabel->move(0,int(ballY + (0 - ballX)*((vy>0)?vy:-vy)/(-vx)));
        ballY = ballY + (0 - ballX)*((vy>0)?vy:-vy)/(-vx);
        ballX = 0;
        vx = -vx;
    }
    if(int(ballX + ballRadius * 2 + vx) > int(windowWidth)){
        ui->ballLabel->move(int(windowWidth - ballRadius*2),int( ballY + (windowWidth - ballRadius*2 - ballX)*((vy>0)?vy:-vy)/(-vx)));
        ballY = ballY + (windowWidth - ballRadius*2 - ballX)*((vy>0)?vy:-vy)/(-vx);
        ballX = windowWidth - ballRadius*2;
        vx = -vx;
    }
    if(int(ballY + vy) < windowHeight*0.11){
        ui->ballLabel->move(int( ballX + (windowHeight*0.11 - ballY)*(((vx>0)?vx:-vx)/(-vy))),int(windowHeight*0.11));
        ballX = ballX + (windowHeight*0.11 - ballY)*(((vx>0)?vx:-vx)/(-vy));
        ballY = windowHeight*0.11;
        vy = -vy;
    }
    if(int(ballY + ballRadius * 2 + vy) >= int(baffleY)){
        if(baffleX > baffleRecordX){
            if((ballX + ballRadius +vx > baffleX + baffleWidth) ||  (ballX + ballRadius +vx < baffleRecordX)){
                if(health>0){
                    health -=1;
                    healthTimer->start(10);
                    launchBall = 1;
                    ballX = baffleX + baffleWidth / 2 - ballRadius;
                    ballY = baffleY - ballRadius * 2;
                    ui->ballLabel->move(int(ballX),int(ballY));
                    ballMoveTimer->stop();
                    clearanceTimer->stop();
                }
                else{
                    ballMoveTimer->stop();
                    collisionTimer->stop();
                    Loseing *l = new Loseing;
                    l->show();
                    this->close();
                    this->deleteLater();
                }
            }
            else{
                ui->ballLabel->move(int( ballX + (baffleY - ballRadius*2 - ballY)*(((vx>0)?vx:-vx)/vy)),int(baffleY - ballRadius*2));
                ballX = ballX + (baffleY - ballRadius*2 - ballY)*(((vx>0)?vx:-vx)/vy);
                ballY = baffleY - ballRadius*2;
                if(vx>=0){
                     vx = (((vx + (baffleX - baffleRecordX)) <= limitX )?(vx + (baffleX - baffleRecordX)) : limitX);
                }
                else{
                    vx = (((vx + (baffleX - baffleRecordX)) >= -limitX )?(vx + (baffleX - baffleRecordX)) : -limitX);
                }
                vy = ((vy>limitY)?(-limitY):-vy);
            }
        }
        if(baffleX <= baffleRecordX){
            if((ballX + ballRadius + vx < baffleX) || (ballX + ballRadius + vx >= baffleRecordX + baffleWidth)){
                if(health>0){
                    health -=1;
                    healthTimer->start(10);
                    launchBall = 1;
                    ballX = baffleX + baffleWidth / 2 - ballRadius;
                    ballY = baffleY - ballRadius * 2;
                    ui->ballLabel->move(int(ballX),int(ballY));
                    ballMoveTimer->stop();
                    clearanceTimer->stop();
                }
                else{
                    ballMoveTimer->stop();
                    collisionTimer->stop();
                    Loseing *l = new Loseing;
                    l->show();
                    this->close();
                    this->deleteLater();
                }
            }
            else{
                ui->ballLabel->move(int( ballX + (baffleY - ballRadius*2 - ballY)*(((vx>0)?vx:-vx)/vy)),int(baffleY - ballRadius*2));
                ballX = ballX + (baffleY - ballRadius*2 - ballY)*(((vx>0)?vx:-vx)/vy);
                ballY = baffleY - ballRadius*2;
                if(vx>=0){
                     vx = (((vx + (baffleX - baffleRecordX)) <= limitX )?(vx + (baffleX - baffleRecordX)) : limitX);
                }
                else{
                    vx = (((vx + (baffleX - baffleRecordX)) >= -limitX )?(vx + (baffleX - baffleRecordX)) : -limitX);
                }
                vy = ((vy>limitY)?(-limitY):-vy);
            }
        }
    }
    else{
        ballX += vx;
        ballY += vy;
        ui->ballLabel->move(int(ballX),int(ballY));
    }
}

//判断障碍物是否被摧毁
void MainWindow::collisionSituation(){
    bool win = true;
    for(int i = 0;i < obstacleNum;i++){
        win = win & obstacleDestoryed[i];
        if(!obstacleDestoryed[i]){
            //右下运动
            if(vx >= 0 && vy >= 0){
                if(ballX + ballRadius*2 + vx >= obstacleX[i] && ballX + ballRadius *2 < obstacleX[i]){
                    if((ballY + ballRadius + vy < obstacleY[i] )&& (ballY + ballRadius * 2 + vy > obstacleY[i])){
                        ui->ballLabel->move(int(obstacleX[i]-ballRadius*2),int((obstacleX[i]-ballRadius*2-ballX) * vy/vx + ballY));
                        ballY = (obstacleX[i]-ballRadius*2-ballX) * vy/vx + ballY;
                        ballX = obstacleX[i]-ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx + 5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballY + ballRadius + vy > obstacleY[i] + obstacleHeight) && (ballY + vy < obstacleY[i] + obstacleHeight)){
                        ui->ballLabel->move(int(obstacleX[i]-ballRadius*2),int((obstacleX[i]-ballRadius*2-ballX) * vy/vx + ballY));
                        ballY = (obstacleX[i]-ballRadius*2-ballX) * vy/vx + ballY;
                        ballX = obstacleX[i]-ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx - 5;
                        vy = vy + 10;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballY + ballRadius + vy >= obstacleY[i]) && (ballY + vy <= obstacleY[i] + obstacleHeight)){
                        ui->ballLabel->move(int(obstacleX[i]-ballRadius*2),int((obstacleX[i]-ballRadius*2-ballX) * vy/vx + ballY));
                        ballY = (obstacleX[i]-ballRadius*2-ballX) * vy/vx + ballY;
                        ballX = obstacleX[i]-ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
                if(ballY + ballRadius*2 + vy >= obstacleY[i] && ballY + ballRadius * 2 < obstacleY[i]){
                    if((ballX + ballRadius + vx < obstacleX[i] ) && (ballX + ballRadius * 2 + vx > obstacleX[i])){
                        ui->ballLabel->move(int((obstacleY[i]-ballRadius*2-ballY) * vx / vy + ballX),int(obstacleY[i]-ballRadius*2));
                        ballX = (obstacleY[i]-ballRadius*2-ballY) * vx / vy + ballX;
                        ballY = obstacleY[i]-ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx + 5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballX + ballRadius + vx > obstacleX[i] + obstacleWidth)&&(ballX + vx < obstacleX[i] + obstacleWidth)){
                        ui->ballLabel->move(int(obstacleY[i] - ballRadius * 2),int((obstacleY[i] - ballRadius * 2 - ballY) * vx/vy + ballX));
                        ballX = (obstacleY[i] - ballRadius * 2 - ballY) * vx/vy + ballX;
                        ballY = obstacleY[i] - ballRadius * 2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx + 5;
                        vy = vy - 10;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballX + ballRadius + vx >= obstacleX[i]) && (ballX + ballRadius + vx <= obstacleX[i] + obstacleWidth)){
                        ui->ballLabel->move(int((obstacleY[i] - ballRadius * 2 - ballY) * vx/vy + ballX),int(obstacleY[i] - ballRadius * 2));
                        ballX = (obstacleY[i] - ballRadius * 2 - ballY) * vx/vy + ballX;
                        ballY = obstacleY[i] - ballRadius * 2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
            }
            //右上运动
            if(vx >= 0&& vy<= 0){
                if(ballX + ballRadius*2 + vx >= obstacleX[i] && ballX + ballRadius * 2<= obstacleX[i]){
                    if((ballY + ballRadius + vy < obstacleY[i])&&(ballY + ballRadius * 2 + vy > obstacleY[i])){
                        ui->ballLabel->move(int(obstacleX[i]-ballRadius*2) , int((obstacleX[i]-ballRadius*2-ballX) * (-vy)/vx + ballY));
                        ballY = (obstacleX[i]-ballRadius*2-ballX) * (-vy)/vx + ballY;
                        ballX = obstacleX[i] - ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx - 5;
                        vy = vy - 10;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballY + ballRadius + vy > obstacleY[i] + obstacleHeight)&&(ballY + vy <obstacleY[i] + obstacleHeight)){
                        ui->ballLabel->move(int(obstacleX[i]-ballRadius*2) , int((obstacleX[i]-ballRadius*2-ballX) * (-vy)/vx + ballY));
                        ballY = (obstacleX[i]-ballRadius*2-ballX) * (-vy)/vx + ballY;
                        ballX = obstacleX[i] - ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx + 5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if(ballY + vy > obstacleY[i] + obstacleHeight&&ballY + 2*vy <= obstacleY[i] + obstacleHeight){
                        ui->ballLabel->move(int((obstacleY[i] + obstacleHeight - ballY) * vx/(-vy) + ballX) , int(obstacleY[i] + obstacleHeight));
                        ballX = (obstacleY[i] + obstacleHeight - ballY) * vx/(-vy) + ballX;
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballY + ballRadius + vy >= obstacleY[i] )&& (ballY + ballRadius + vy <= obstacleY[i] + obstacleHeight)){
                        ui->ballLabel->move(int(obstacleX[i]-ballRadius*2) , int((obstacleX[i]-ballRadius*2-ballX) * (-vy)/vx + ballY));
                        ballY = (obstacleX[i]-ballRadius*2-ballX) * (-vy)/vx + ballY;
                        ballX = obstacleX[i] - ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
                if(ballY + vy >= obstacleY[i] + obstacleHeight && ballY < obstacleY[i]+obstacleHeight){
                    if((ballX + ballRadius + vx > obstacleX[i] + obstacleWidth)&&(ballX + vx < obstacleX[i] + obstacleWidth)){
                        ui->ballLabel->move(int (ballX + (obstacleY[i]+obstacleHeight-ballY)*vx/(-vy)) , int(obstacleY[i]+obstacleHeight));
                        ballX = ballX + (obstacleY[i]+ballRadius*2-ballY)*vx/(-vy);
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx + 10;
                        vy = vy + 5;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballX + ballRadius + vx < obstacleX[i])&&(ballX + ballRadius * 2>obstacleX[i])){
                        ui->ballLabel->move(int (ballX + (obstacleY[i]+obstacleHeight-ballY)*vx/(-vy)) , int(obstacleY[i]+obstacleHeight));
                        ballX = ballX + (obstacleY[i]+ballRadius*2-ballY)*vx/(-vy);
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx + 5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballX + ballRadius + vx >= obstacleX[i] )&& (ballX + ballRadius + vx <= obstacleX[i] + obstacleWidth)){
                        ui->ballLabel->move(int (ballX + (obstacleY[i]+obstacleHeight-ballY)*vx/(-vy)) , int(obstacleY[i]+obstacleHeight));
                        ballX = ballX + (obstacleY[i]+ballRadius*2-ballY)*vx/(-vy);
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vy=-vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    /*if(ballX + ballRadius * 2+ vx < obstacleX[i]&&ballX + ballRadius * 2+ 2*vx >= obstacleX[i]){
                        ui->ballLabel->move(int (obstacleX[i]-ballRadius*2) , int( ballY + (obstacleX[i]-ballRadius*2-ballX)*(-vy)/vx));
                        ballY = ballY + (obstacleX[i]-ballRadius*2-ballX)*(-vy)/vx;
                        ballX = obstacleX[i]-ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&level_now != 1){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }*/
                }
            }
            //左下运动
            if(vx <= 0 && vy >= 0){
                if(ballX + vx <= obstacleX[i] + obstacleWidth && ballX > obstacleX[i] + obstacleWidth ){
                    if((ballY + ballRadius + vy > obstacleY[i] + obstacleHeight)&&(ballY < obstacleY[i] + obstacleHeight)){
                        ui->ballLabel->move(int(obstacleX[i]+obstacleWidth) , int((obstacleX[i]+obstacleWidth -ballX) * vy/(-vx) + ballY));
                        ballY = (obstacleX[i] + obstacleWidth -ballX) * vy/(-vx) + ballY;
                        ballX = obstacleX[i] + obstacleWidth;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx + 5;
                        vy = vy + 10;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballY + ballRadius * 2 + vy > obstacleY[i])&&(ballY + ballRadius + vy < obstacleY[i])){
                        ui->ballLabel->move(int(obstacleX[i]+obstacleWidth) , int((obstacleX[i]+obstacleWidth -ballX) * vy/(-vx) + ballY));
                        ballY = (obstacleX[i] + obstacleWidth -ballX) * vy/(-vx) + ballY;
                        ballX = obstacleX[i] + obstacleWidth;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx + 5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if(ballY + ballRadius + vy <= obstacleY[i] + obstacleHeight && ballY + ballRadius + vy >= obstacleY[i]){
                        ui->ballLabel->move(int(obstacleX[i]+obstacleWidth) , int((obstacleX[i]+obstacleWidth -ballX) * vy/(-vx) + ballY));
                        ballY = (obstacleX[i] + obstacleWidth -ballX) * vy/(-vx) + ballY;
                        ballX = obstacleX[i] + obstacleWidth;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
                if(ballY + ballRadius*2 + vy >= obstacleY[i] && ballY + ballRadius*2 < obstacleY[i]){
                    if((ballX + ballRadius + vx < obstacleX[i])&&(ballX + ballRadius * 2+ vx> obstacleX[i])){
                        ui->ballLabel->move(int(ballX + (obstacleY[i] - ballRadius*2-ballY)*(-vx)/vy), int(obstacleY[i] - ballRadius*2));
                        ballX = ballX + (obstacleY[i] - ballRadius*2-ballY)*(-vx)/vy;
                        ballY = obstacleY[i] - ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx - 10;
                        vy = vy - 5;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballX + ballRadius + vx > obstacleX[i] + obstacleWidth) && (ballX + vx < obstacleX[i] + obstacleWidth)){
                        ui->ballLabel->move(int(ballX + (obstacleY[i] - ballRadius*2-ballY)*(-vx)/vy), int(obstacleY[i] - ballRadius*2));
                        ballX = ballX + (obstacleY[i] - ballRadius*2-ballY)*(-vx)/vy;
                        ballY = obstacleY[i] - ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx + 5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if(ballX + ballRadius + vx <= obstacleX[i] + obstacleWidth && ballX + ballRadius + vx >= obstacleX[i]){
                        ui->ballLabel->move(int(ballX + (obstacleY[i] - ballRadius*2-ballY)*(-vx)/vy), int(obstacleY[i] - ballRadius*2));
                        ballX = ballX + (obstacleY[i] - ballRadius*2-ballY)*(-vx)/vy;
                        ballY = obstacleY[i] - ballRadius*2;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vy=-vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
            }
            //左上运动
            if(vx <= 0 && vy <= 0){
                if(ballX + vx <= obstacleX[i] + obstacleWidth && ballX > obstacleX[i]+obstacleWidth){
                    if((ballY + ballRadius + vy <obstacleY[i])&&(ballY + ballRadius * 2 + vy > obstacleY[i])){
                        ui->ballLabel->move(int(obstacleX[i]+obstacleWidth ) , int((obstacleX[i]+obstacleWidth - ballX) * vy/vx +ballY));
                        ballY = (obstacleX[i]+obstacleWidth - ballX) * vy/vx +ballY;
                        ballX = obstacleX[i] + obstacleWidth;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx + 5;
                        vy = vy - 10;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballY + ballRadius + vy > obstacleY[i] + obstacleHeight)&&(ballY + vy < obstacleY[i] + obstacleHeight)){
                        ui->ballLabel->move(int(obstacleX[i]+obstacleWidth ) , int((obstacleX[i]+obstacleWidth - ballX) * vy/vx +ballY));
                        ballY = (obstacleX[i]+obstacleWidth - ballX) * vy/vx +ballY;
                        ballX = obstacleX[i] + obstacleWidth;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = - vx -5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if(ballY + ballRadius + vy <= obstacleY[i] + obstacleHeight && ballY + ballRadius + vy >= obstacleY[i] ){
                        ui->ballLabel->move(int(obstacleX[i]+obstacleWidth ) , int((obstacleX[i]+obstacleWidth - ballX) * vy/vx +ballY));
                        ballY = (obstacleX[i]+obstacleWidth - ballX) * vy/vx +ballY;
                        ballX = obstacleX[i] + obstacleWidth;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = -vx;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
                if(ballY + vy <= obstacleY[i] + obstacleHeight && ballY > obstacleY[i]+obstacleHeight){
                    if((ballX + ballRadius + vx < obstacleX[i])&&(ballX + ballRadius * 2 + vx > obstacleX[i])){
                        ui->ballLabel->move(int( ballX + (obstacleY[i] + obstacleHeight-ballY)*vx/vy)  ,  int(obstacleY[i] + obstacleHeight));
                        ballX = ballX + (obstacleY[i] + obstacleHeight-ballY)*vx/vy;
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = vx - 10;
                        vy = vy + 5;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if((ballX + ballRadius + vx > obstacleX[i] + obstacleWidth)&&(ballX + vx < obstacleX[i]+ obstacleWidth)){
                        ui->ballLabel->move(int( ballX + (obstacleY[i] + obstacleHeight-ballY)*vx/vy)  ,  int(obstacleY[i] + obstacleHeight));
                        ballX = ballX + (obstacleY[i] + obstacleHeight-ballY)*vx/vy;
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vx = - vx -5;
                        vy = -vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                    if(ballX + ballRadius + vx >= obstacleX[i] && ballX + ballRadius+ vx<= obstacleX[i] + obstacleWidth){
                        ui->ballLabel->move(int( ballX + (obstacleY[i] + obstacleHeight-ballY)*vx/vy)  ,  int(obstacleY[i] + obstacleHeight));
                        ballX = ballX + (obstacleY[i] + obstacleHeight-ballY)*vx/vy;
                        ballY = obstacleY[i] + obstacleHeight;
                        flashBallX = ballX;
                        flashBallY = ballY;
                        flashTimes = 1;
                        flashBallTimer->start(flashBallTimerFPS);
                        vy=-vy;
                        obstacleDestoryed[i] = true;
                        obstacleLabel[i]->hide();
                        if(i==2&&(level_now != 1 && level_now != 9)){
                            specialTimer_1->start(1);
                        }
                        if(i==20){
                            specialTimer_2->start(laserFPS);
                        }
                    }
                }
            }
        }
    }

    //判断是否通关
    if(win){
        if(level_now < 9){      
            QSqlDatabase myleveldb;
            /*if(QSqlDatabase::contains("qt_sql_default_connection")){
                qDebug()<<"cao";
            }*/
            myleveldb = QSqlDatabase::database("qt_sql_default_connection");
            QSqlQuery query;
            int level_open = level_now - 1;
            QString sqlquery = QString("insert into LEVEL values('%1','%2')").arg(level_open).arg(0);
            query.prepare(sqlquery);
            if(query.exec()){
               qDebug()<<"create person success!";
            }
            if(!query.exec()){
                qDebug()<<"fuck";
            }
            myleveldb.close();
            collisionTimer->stop();
            ballMoveTimer->stop();
            clearanceTimer->stop();
            Clearance *c = new Clearance(nullptr,level_now);
            c->show();
            this->close();
            this->deleteLater();
        }
        else{
            collisionTimer->stop();
            ballMoveTimer->stop();
            clearanceTimer->stop();
            victory *v = new victory;
            v->show();
            this->close();
            this->deleteLater();
        }
    };
}

//空格键暂停事件
void MainWindow::keyPressEvent(QKeyEvent *k){
    if(k->key() == Qt::Key_Space && !interval){
        ballMoveTimer->stop();
        collisionTimer->stop();
        clearanceTimer->stop();
        interval = true;
    }
    else{
        ballMoveTimer->start(ballMoveTimerFPS);
        collisionTimer->start(collisionTimerFPS);
        clearanceTimer->start(clearanceTimerFPS);
        interval = false;
    }
}

//通关倒计时定时器
void MainWindow::clearanceTimerEvent()
{
    limitTime -= 1;
    ui->timeLabel->setText("剩余时间:"+QString::number(limitTime));
    if(limitTime <= 0){
        ballMoveTimer->stop();
        collisionTimer->stop();
        Loseing *l = new Loseing;
        l->show();
        this->close();
        this->deleteLater();
    }
}

//生命值显示
void MainWindow::healthShow(){
    if(health == 2){
        ui->healthLabel->setPixmap(QPixmap(":/new/characters/images/characters/health3.png"));
    }
    if(health == 1){
        ui->healthLabel->setPixmap(QPixmap(":/new/characters/images/characters/health2.png"));
    }
    if(health == 0){
        ui->healthLabel->setPixmap(QPixmap(":/new/characters/images/characters/health1.png"));
    }
    healthTimer->stop();
}

//点击暂停事件
void MainWindow::on_intervalButton_clicked()
{
    if(!interval){
        ballMoveTimer->stop();
        collisionTimer->stop();
        clearanceTimer->stop();
        interval = true;
    }
    else{
        ballMoveTimer->start(ballMoveTimerFPS);
        collisionTimer->start(collisionTimerFPS);
        clearanceTimer->start(clearanceTimerFPS);
        interval = false;
    }
}

//点击返回主菜单|home键
void MainWindow::on_homeButton_clicked()
{
    StartInterface *s = new StartInterface(nullptr,true);
    s->show();
    this->close();
    this->deleteLater();
}

//球残留事件
void MainWindow::flashBallEvent(){
    ui->ballLabel2->move(int(flashBallX),int(flashBallY));
    if((ballX-flashBallX)>=50 || (ballX-flashBallX)<=-50){
        ui->ballLabel2->hide();
        flashBallTimer->stop();
    }
    else{
        if(flashTimes == 1){
            ui->ballLabel2->show();
        }
        if(flashTimes == 4){
            ui->ballLabel2->hide();
        }
        if(flashTimes == 7){
            ui->ballLabel2->show();
        }
        if(flashTimes == 10){
            ui->ballLabel2->hide();
            flashBallTimer->stop();
        }
    }
    flashTimes += flashTimes % 10 + 1;
}

//特殊砖块
//1挡板变长
void MainWindow::special_1(){
    baffleWidth = baffleWidth * 2;
    ui->baffleLabel->setGeometry(int(baffleX),int(baffleY),int(baffleWidth),int(baffleHeight));
    specialTimer_1->stop();
}
//2发射激光
void MainWindow::special_2(){
    ui->baffleLabel->setPixmap(QPixmap(":/new/characters/images/characters/bafflePicture2.png"));
    ui->laserLabel->show();
    if(laserCondition == 1){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser1.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 2){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser2.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 3){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser3.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 4){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser4.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 5){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser5.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 6){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser6.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 7){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser7.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 8){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser8.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 9){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser9.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 10){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser10.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 11){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser11.png"));
        laserCondition = laserCondition%6+1;
    }
    if(laserCondition == 12){
        ui->laserLabel->setPixmap(QPixmap(":/new/characters/images/characters/laser12.png"));
        laserCondition = laserCondition%6+1;
    }
    for(int i = 0;i<obstacleNum;i++){
        if(!obstacleDestoryed[i]){
            if(baffleX +baffleWidth/2 >= obstacleX[i] && baffleX + baffleWidth/2 <= obstacleX[i] + obstacleWidth){
                obstacleDestoryed[i] = true;
                obstacleLabel[i]->hide();
                if(i==2&&(level_now != 1 && level_now != 9)){
                    specialTimer_1->start(1);
                }
                if(i==7){
                    specialTimer_2->start(laserFPS);
                }
            }
        }
    }
    specialTimer_2->start(laserFPS);
}

