#include "level.h"
#include "ui_level.h"
#include "startinterface.h"


Level::Level(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Level)
{

    QSqlDatabase myleveldb;
    if(QSqlDatabase::contains("qt_sql_default_connection")){
        myleveldb = QSqlDatabase::database("qt_sql_default_connection");
        qDebug()<<"link success";
    }
    else{
        myleveldb = QSqlDatabase::addDatabase("QSQLITE");
        myleveldb.setDatabaseName("level.db");
        myleveldb.setUserName("liaobiyan");
        myleveldb.setPassword("123456");
        bool b = myleveldb.open();
        if(!b){
            qDebug()<<"error";
        }
        else{
            QSqlQuery query;
            QString creatTableStr = "CREATE TABLE LEVEL \
                    (                                   \
                        level_  int(8)   NOT NULL,       \
                        blocked_  int(2)   NOT NULL      \
                    );";
            query.prepare(creatTableStr);
            if(!query.exec()){
                qDebug()<<"link success";
            }
            else{
                qDebug()<<"creat table success!";
            }
        }
        myleveldb.close();
    }
    myleveldb.open();
        QSqlQuery query1;
        query1.exec("select * from LEVEL");
        while(query1.next()){
            int i = query1.value(0).toString().toInt();
            blocked[i] = query1.value(1).toString().toInt();
            //qDebug()<<query1.value(0).toString()<<" "<<query1.value(1).toString();
        }
        if(query1.exec()){
            qDebug()<<"update date success";
        }
    myleveldb.close();


    ui->setupUi(this);
    resize(int(600),int(600*1.44));

    ui->pushButton_10->setGeometry(0 , 0,int(0.243*600),int(0.125*600*1.44));
    ui->pushButton_10->setIcon(QPixmap(":/new/icons/images/icons/back.png"));
    ui->pushButton_10->setIconSize(QSize(int(0.243*600),int(0.125*600*1.44)));
    ui->pushButton_10->setStyleSheet("QPushButton{border:0px}");

    ui->pushButton->setGeometry(int(0.14*this->width()) , int(0.31*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton->setIcon(QPixmap(":/new/icons/images/icons/level1.png"));
    ui->pushButton->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton->setStyleSheet("QPushButton{border:0px}");

    ui->pushButton_2->setGeometry(int(0.356*this->width()+0.025*this->height()),int( 0.31*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_2->setIcon(QPixmap(":/new/icons/images/icons/level2.png"));
    ui->pushButton_2->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_2->setStyleSheet("QPushButton{border:0px}");

    ui->block2->setGeometry(int(0.572*this->width()+0.025*this->height()-25),int( 0.36*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block2->setPixmap(QPixmap(":/new/icons/whole/images/icons/blocked.png"));
    ui->block2->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block2->setStyleSheet("QLabel{border:0px}");

    ui->pushButton_3->setGeometry(int(0.572*this->width()+0.05*this->height()), int(0.31*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_3->setIcon(QPixmap(":/new/icons/images/icons/level3.png"));
    ui->pushButton_3->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_3->setStyleSheet("QPushButton{border:0px}");

    ui->block3->setGeometry(int(0.788*this->width()+0.025*this->height()-20),int( 0.36*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block3->setPixmap(QPixmap(":/new/icons/whole/images/icons/blocked.png"));
    ui->block3->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block3->setStyleSheet("QLabel{border:0px}");

    ui->pushButton_4->setGeometry(int(0.14*this->width()), int(0.486*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_4->setIcon(QPixmap(":/new/icons/images/icons/level4.png"));
    ui->pushButton_4->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_4->setStyleSheet("QPushButton{border:0px}");

    ui->block4->setGeometry(int(0.256*this->width()+0.025*this->height()),int( 0.536*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block4->setPixmap(QPixmap(":/new/icons/whole/images/icons/blocked.png"));
    ui->block4->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block4->setStyleSheet("QLabel{border:0px}");


    ui->pushButton_5->setGeometry(int(0.356*this->width()+0.025*this->height()), int(0.486*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_5->setIcon(QPixmap(":/new/icons/images/icons/level5.png"));
    ui->pushButton_5->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_5->setStyleSheet("QPushButton{border:0px}");

    ui->block5->setGeometry(int(0.572*this->width()+0.025*this->height()-25),int( 0.536*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block5->setPixmap(QPixmap(":/new/icons/images/icons/blocked.png"));
    ui->block5->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block5->setStyleSheet("QLabel{border:0px}");

    ui->pushButton_6->setGeometry(int(0.572*this->width()+0.05*this->height()) , int(0.486*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_6->setIcon(QPixmap(":/new/icons/images/icons/level6.png"));
    ui->pushButton_6->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_6->setStyleSheet("QPushButton{border:0px}");

    ui->block6->setGeometry(int(0.788*this->width()+0.025*this->height()-20),int( 0.536*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block6->setPixmap(QPixmap(":/new/icons/images/icons/blocked.png"));
    ui->block6->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block6->setStyleSheet("QLabel{border:0px}");

    ui->pushButton_7->setGeometry(int(0.14*this->width()),int(0.662*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_7->setIcon(QPixmap(":/new/icons/images/icons/level7.png"));
    ui->pushButton_7->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_7->setStyleSheet("QPushButton{border:0px}");

    ui->block7->setGeometry(int(0.256*this->width()+0.025*this->height()),int( 0.712*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block7->setPixmap(QPixmap(":/new/icons/images/icons/blocked.png"));
    ui->block7->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block7->setStyleSheet("QLabel{border:0px}");

    ui->pushButton_8->setGeometry(int(0.356*this->width()+0.025*this->height()) ,int(0.662*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_8->setIcon(QPixmap(":/new/icons/images/icons/level8.png"));
    ui->pushButton_8->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_8->setStyleSheet("QPushButton{border:0px}");

    ui->block8->setGeometry(int(0.572*this->width()+0.025*this->height()-25),int( 0.712*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block8->setPixmap(QPixmap(":/new/icons/images/icons/blocked.png"));
    ui->block8->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block8->setStyleSheet("QLabel{border:0px}");

    ui->pushButton_9->setGeometry(int(0.572*this->width()+0.05*this->height()),int(0.662*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->pushButton_9->setIcon(QPixmap(":/new/icons/images/icons/level9.png"));
    ui->pushButton_9->setIconSize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->pushButton_9->setStyleSheet("QPushButton{border:0px}");

    ui->block9->setGeometry(int(0.788*this->width()+0.025*this->height()-20),int( 0.712*this->height()),int(0.08*600),int(0.07*600*1.44));
    ui->block9->setPixmap(QPixmap(":/new/icons/images/icons/blocked.png"));
    ui->block9->resize(QSize(int(0.216*600),int(0.151*600*1.44)));
    ui->block9->setStyleSheet("QLabel{border:0px}");

    ui->mouthLabel->setGeometry(int(0.057*this->width()),int(0.716*this->height()),int(0.216*600),int(0.151*600*1.44));
    ui->mouthLabel->setPixmap(QPixmap(":/new/characters/images/characters/mouthPicture.png"));
    ui->mouthLabel->setScaledContents(true);
    ui->mouthLabel->raise();

    block[0] = ui->block2;
    block[1] = ui->block3;
    block[2] = ui->block4;
    block[3] = ui->block5;
    block[4] = ui->block6;
    block[5] = ui->block7;
    block[6] = ui->block8;
    block[7] = ui->block9;
    for(int i = 0;i<8;i++){
        if(blocked[i]==0){
            block[i]->hide();
        }
    }
}

Level::~Level()
{
    delete ui;
}

void Level::paintEvent(QPaintEvent *){
    QPainter painter(this);
    QPixmap p;
    p.load(":/new/backgrounds/images/backgrounds/levelPicture.png");
    painter.drawPixmap(0,0,this->width(),this->height(),p);
}


void Level::on_pushButton_clicked()
{
    level = 1;
    MainWindow *m = new MainWindow(nullptr,1);
    m->show();
    this->close();
    this->deleteLater();
}

void Level::on_pushButton_2_clicked()
{
    level = 2;
    if(blocked[0]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_3_clicked()
{
    level = 3;
    if(blocked[1]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_4_clicked()
{
    level = 4;
    if(blocked[2]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_5_clicked()
{
    level = 5;
    if(blocked[3]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_6_clicked()
{
    level = 6;
    if(blocked[4]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_7_clicked()
{
    level = 7;
    if(blocked[5]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_8_clicked()
{
    level = 8;
    if(blocked[6]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_9_clicked()
{
    level = 9;
    if(blocked[7]==0){
        MainWindow *m = new MainWindow(nullptr,level);
        m->show();
        this->close();
        this->deleteLater();
    }
}

void Level::on_pushButton_10_clicked()
{
    StartInterface *s = new StartInterface(nullptr,true);
    s->show();
    this->close();
    this->deleteLater();
}
