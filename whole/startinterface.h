#ifndef STARTINTERFACE_H
#define STARTINTERFACE_H
#include "level.h"
#include <QDialog>
#include <QPalette>
#include <QPainter>
#include <QPixmap>
#include <QPushButton>
#include <QPropertyAnimation>
namespace Ui {
class StartInterface;
}

class StartInterface : public QDialog
{
    Q_OBJECT

public:
    explicit StartInterface(QWidget *parent = nullptr, bool haveread = false);
    ~StartInterface();
    bool haveread_now;
    void paintEvent(QPaintEvent *);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::StartInterface *ui;
};

#endif // STARTINTERFACE_H
