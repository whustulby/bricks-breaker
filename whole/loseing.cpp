#include "loseing.h"
#include "ui_loseing.h"
#include "startinterface.h"
Loseing::Loseing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Loseing)
{
    ui->setupUi(this);
}

Loseing::~Loseing()
{
    delete ui;
}

void Loseing::on_pushButton_clicked()
{
    StartInterface *s = new StartInterface(nullptr,true);
    s->show();
    this->close();
    this->deleteLater();
}
